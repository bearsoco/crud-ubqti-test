import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatDialogConfig} from '@angular/material';
import { ModalDetailsComponent } from './modal-details/modal-details.component';
import { ModalService } from '../datatable/service/modal.service'; 
import { RestService } from "../../rest.service";

export interface DataTable {
  name: any;
  last_name: any;
  birthdate: any;
  email: any;
  phone: any;
  profession: any;
  address: any;
}

let users: any[] = [];

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})

export class DatatableComponent implements OnInit {
  displayedColumns: string[] = ['name', 'last_name', 'phone', 'details', 'edit', 'delete'];
  dataSource: MatTableDataSource<DataTable>;
  resultado: any;
  datos: DataTable;
  ids: any[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dialog: MatDialog,
              private service: ModalService,
              public restService: RestService) {
    this.loadListUsers();
  } 

  ngOnInit() {
    this.dataSource.paginator = this.paginator;

  }

  loadListUsers() {
  var guille;
  var key
  this.restService.getUsers().then(result => {    
    guille = result;
    console.log("PRUEBAS");
    console.log(guille.response.data);
    
    for (let i = 0; i < guille.response.data.length; i++){
      for (key in guille.response.data[i]){
        var id = key
        console.log(id);
      }
      this.datos = {
        name: guille.response.data[i][id].name,
        last_name: guille.response.data[i][id].last_name,
        birthdate: guille.response.data[i][id].birthdate,
        email: guille.response.data[i][id].email,
        phone: guille.response.data[i][id].phone,
        profession: guille.response.data[i][id].info.profession,
        address: guille.response.data[i][id].info.address,
      };
      console.log(this.datos);
      users.push(this.datos);

  }
      
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
    },
    err => {
      console.log(err);
    }
  );
}


  openDialogDetails(row){
    //this.service.populateForm(row);

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '70%';
    
    //dialogConfig.data = row;
    this.service.disableForm();

    
    this.dialog.open(ModalDetailsComponent, dialogConfig);

        
  }

  openDialogEdit(row){
    this.service.initializeFormGroup();

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '70%';
    this.dialog.open(ModalDetailsComponent, dialogConfig);
    this.service.enableEditables();
    

  }

  openDialogDelete(row){

  }

}
