import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as Chart from 'chart.js';
import { ModalService } from '../../datatable/service/modal.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modal-details',
  templateUrl: './modal-details.component.html',
  styleUrls: ['./modal-details.component.css']
})
export class ModalDetailsComponent implements OnInit {

  DoughnutChart = [];
  
  
  constructor(private service: ModalService,
              public dialogRef: MatDialogRef<ModalDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { 
    
              }

  form = new FormControl('', [
    Validators.required
  ]);

  ngOnInit() {
    this.DoughnutChart = new Chart('DoughnutChart', {
      type: 'doughnut',
      data: {
        labels: ['Skill1', 'Skill2', 'Skill3', 'Skill4', 'Skill5', 'Skill6'],
        datasets: [{
            label: '% de Habilidades',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
        }]
    },
    options: {
      plugins: {
        datalabels: {
          color: '#000000',
          formatter: function(value) {
            return Math.round(value);
          },
          font: {
            weight: 'bold',
            size: 16,
          }
        },
   
    },

    legend: {
      position: 'top',
      fullWidth: true
    },
  }  });

  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
}

  onClose() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    this.dialogRef.close();
}
 
}
