import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import * as _ from 'lodash';
@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    name: new FormControl('', Validators.required),
    last_name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    phone: new FormControl('', [Validators.required, Validators.minLength(8)]),
    age: new FormControl(''),
    profession: new FormControl(0),
    address: new FormControl('')
  });

  initializeFormGroup() {
    this.form.setValue({
      $key: null,
      name: '',
      last_name: '',
      email: '',
      phone: '',
      age: 0,
      profession: '',
      address: ''
  
    });
}

disableForm() {
  this.form.disable()
 }  

enableEditables(){
  this.form.controls['name'].disable();
  this.form.controls['last_name'].disable();
  this.form.controls['email'].enable();
  this.form.controls['phone'].enable();
  this.form.controls['age'].disable();
  this.form.controls['profession'].disable();
  this.form.controls['address'].enable();
}

populateForm(user) {
  this.form.setValue(user);
}


}
